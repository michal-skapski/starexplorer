﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FoxBehaviour : MonoBehaviour
{
    [SerializeField] private Quest _questObjectManager;

    [SerializeField] private AudioSource _runSound;
    [SerializeField] private AudioSource _catchSound;
    [SerializeField] private GameObject _foxModel;
    [SerializeField] private GameObject _player;
    private float _calmingTime = 30f;
    // [SerializeField] private Vector3 _darkLanePos;
    private Rigidbody _myRb;
    private NavMeshAgent _navMeshAgent;
    private Vector3 _foxRestingPos;
    private Vector3 _playerFoxPos;
    private Vector3 _escapingPos;
    
    private float _distanceToFox;
    private float _zeroVal = 0;
    private float _catchDistance = 5f;
    private float _detectionDistance = 30f;
    private float _fleeingDistance = 20f;

    private bool _runSoundCanPlay;
    public bool foxCanBeCatched;

    [SerializeField] private Animator _myAnim;
    
    private const string _lookBool = "look";
    private Vector3 _lookTowards = new Vector3(1, 1, -1);

    private IEnumerator SettlingDownChecker() // Coroutines for Fox returning to it's original position
    {
        yield return new WaitForSecondsRealtime(_calmingTime);
        if (_distanceToFox > _fleeingDistance)
        {
            _navMeshAgent.SetDestination(_foxRestingPos);
        }
    }

    private void Detection()
    {
        _distanceToFox = Vector3.Distance(transform.position, _player.transform.position); // distance beetween the fox and the player

        if (_distanceToFox < _detectionDistance)
        {
            transform.LookAt(_player.transform);
            _runSoundCanPlay = true;
            // Run detection animation
        }
    }
    private void Flee()
    {
        if(_runSoundCanPlay == true)
        {
            _runSound.Play();
        }

        _playerFoxPos = transform.position - _player.transform.position;
        _escapingPos = transform.position + _playerFoxPos;
        _navMeshAgent.SetDestination(_escapingPos);
        StartCoroutine(SettlingDownChecker());
    }
    private void CatchFox()
    {
        if (foxCanBeCatched && _distanceToFox < _catchDistance && Input.GetKeyDown(KeyCode.E))
        {
            _questObjectManager.GoalReached();
            this.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        _runSoundCanPlay = false;
        _catchSound.Play();
        _myAnim.SetBool(_lookBool, true); // Run smiling animation


        _foxModel.transform.localScale = _lookTowards;

        this._navMeshAgent.speed = _zeroVal; // prevents fox from moving
        _myRb.detectCollisions = false;
        _myRb.constraints = RigidbodyConstraints.FreezeAll; // secure for fox not falling
        foxCanBeCatched = true;

    }
    private void Awake()
    {
        _runSoundCanPlay = false;
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _myRb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        _foxRestingPos = transform.position;
    }
    private void Update()
    {
        Detection();
        if(_distanceToFox < _fleeingDistance)
        {
            Flee();
        }
        CatchFox();
    }
}