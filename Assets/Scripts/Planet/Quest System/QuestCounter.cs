﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCounter : MonoBehaviour
{
    private static int _questAmount;
    [HideInInspector] public static int currentDoneAmount = 0;
    private static int _oneValue = 1;

    private void Awake()
    {
        _questAmount = transform.childCount;
    }
    public static void ChangeStatus()
    {
        currentDoneAmount += _oneValue;
        if (_questAmount <= currentDoneAmount)
        {
            InfoBannerAnimCont.TriggerAnimation(); //call method in InfoBannerAnimCont to trigger animation

            TriggerEnd.ShipTriggerOn(); //calls method in TriggerEnd to turn on trigger collider on spaceship
        }
    }
}
