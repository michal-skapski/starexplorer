﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanHolder : MonoBehaviour
{
    [SerializeField] private Quest _questSlot;
    private GameObject[] _thisChildArray;
    private int _scanCount;
    private int _oneNum = 1;

    private void Awake()
    {
        _thisChildArray = new GameObject[transform.childCount]; //make an array size of this object's child amount (this is for automatically getting number of quests)
        _scanCount = 0;
    }

    public void ObjectScanned() //when called adds one to total count of scanned objects by player
    {
        _scanCount += _oneNum;

        if (_scanCount >= _thisChildArray.Length) //when total count of scanned objects is equal or bigger than the amount of all scannable objects (_thisChildArray) call method in Quest class that goal has been reached by player
        {
            _questSlot.GoalReached();
        }
    }
}
