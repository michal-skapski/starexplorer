﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scan_q : MonoBehaviour
{
    [SerializeField] private ScanHolder _addScanned; //or later change to get reference from "grandpa"(?) of this object
    [SerializeField] private Collider _playerCollider;
    [SerializeField] private Animator _scanerAnimator;
    [SerializeField] private GameObject _thisObjCopy;
    [SerializeField] private Camera _mainCamera; //find a way to automatize getting reference of this
    private AudioSource _scanSound;

    private Transform _thisTransform;
    private bool _isPlayerLooking;
    private string _animatorBoolName = "isScanning";
    private float _waitTimeScan = 2.5f;

    private void ObjectsActivator(bool originalObject, bool copyObject)
    {
        this.gameObject.SetActive(originalObject);
        _thisObjCopy.SetActive(copyObject);
    }

    private void Awake()
    {
        _scanSound = GetComponent<AudioSource>();
        _thisTransform = GetComponent<Transform>();
        _isPlayerLooking = false;
        ObjectsActivator(true, false);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other == _playerCollider)
        {
            var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                var selection = hit.transform;

                if (selection == _thisTransform && !_isPlayerLooking) //call when player looks at cube
                {
                    _isPlayerLooking = true;
                    StartCoroutine(Scan());
                }
            }
        }
    }

    private IEnumerator Scan()
    {
        _scanerAnimator.SetBool(_animatorBoolName, true); //turn on scan animation
        _scanSound.Play();
        yield return new WaitForSeconds(_waitTimeScan);
        _scanerAnimator.SetBool(_animatorBoolName, false); //turn off scan animation

        ObjectsActivator(false, true); //switch scanned object to unscannable visual copy
        _addScanned.ObjectScanned(); //call method ObjectScanned() in ScanHolder script
    }
}
