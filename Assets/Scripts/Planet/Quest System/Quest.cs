﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Quest : MonoBehaviour
{
    [SerializeField] private string _questTitle = null; //set quest name in inspector // nulls for unity not showing error
    [SerializeField] private GameObject _questObject = null; //for quest object itself
    private TextMeshProUGUI _thisTMP;

    private string _questComplete = "QUEST COMPLETED";

    private void Awake()
    {
        _thisTMP = this.GetComponent<TextMeshProUGUI>();
        _thisTMP.text = _questTitle; //change displayed quest name to what's set in inspector _questTitle
    }

    public void GoalReached()
    {
        _thisTMP.text = _questComplete;
        QuestCounter.ChangeStatus();
    }
}
