﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [SerializeField] private Transform _playerBody;
    [SerializeField] private float _mouseSensivity;
    private float _mouseX;
    private float _mouseY;
    private float _xRotation = 0f;

    private float _zeroVal = 0;
    private float _ninetyVal = 90f;
    private string _mouseXAxisName = "Mouse X";
    private string _mouseYAxisName = "Mouse Y";


    private void InputActivators()
    {
        _mouseX = Input.GetAxis(_mouseXAxisName) * _mouseSensivity * Time.deltaTime;
        _mouseY = Input.GetAxis(_mouseYAxisName) * _mouseSensivity * Time.deltaTime;
    }
    private void CameraRotation()
    {
        _xRotation -= _mouseY;
        _xRotation = Mathf.Clamp(_xRotation, -_ninetyVal, _ninetyVal);
        _playerBody.Rotate(Vector3.up * _mouseX);
        transform.localRotation = Quaternion.Euler(_xRotation, _zeroVal, _zeroVal);
    }
    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
    private void Update()
    {
        InputActivators();
        CameraRotation();
    }
}
