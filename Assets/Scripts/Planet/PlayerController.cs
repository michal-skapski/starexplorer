﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private AudioSource _feetSound;
    [SerializeField] private Transform _groundChecker;
    [SerializeField] private float _movementSpeed;
    [SerializeField] private LayerMask _groundMask;
    [SerializeField] private float _gravityForce;
    [SerializeField] private float _jumpHeight;
    [SerializeField] private float _toLandTime = 1f;
    private float _groundDistance=0.4f;    
    private bool _isGrounded;
    private bool _feetCanPlay;
    private CharacterController _playerController;

    private float _xInput;
    private float _zInput;

    private string _horizontalAxisName = "Horizontal";
    private string _verticalAxisName = "Vertical";

    private Vector3 _direction;

    private float _zeroVal = 0f;
    private float _negativeTwoVal = -2f;
    private Vector3 _velocity;
    private float _overallSpeed;
    [SerializeField] private Animator _foxAnim;
    [SerializeField] private Transform _foxModel;
    private string _lookBool="look";
    private string _runTrigger = "run";

    private Vector3 _foxLookTowards = new Vector3(1, 1, -1);
    private Vector3 _foxLookBackwards = new Vector3(1, 1, 1);

    private void OnTriggerEnter(Collider other)
    {
        _foxModel.transform.localScale = _foxLookTowards; // fox turning towards us
        _foxAnim.SetBool(_lookBool, true);
    }
    private void OnTriggerExit(Collider other)
    {
        _foxModel.transform.localScale = _foxLookBackwards; // fox turning away from us
        _foxAnim.SetBool(_lookBool, false);
        _foxAnim.SetTrigger(_runTrigger);
    }
    private IEnumerator SoundJumpOperator()
    {
        _feetCanPlay = false;
        yield return new WaitForSecondsRealtime(_toLandTime);
        _feetCanPlay = true;
    }
    private void AudioOperator()
    {
        if (_overallSpeed != _zeroVal && _feetCanPlay == true)
        {
            _feetSound.UnPause();
        }
        else
        {
            _feetSound.Pause();
        }
    }
    private void InputActivators()
    {
        _xInput = Input.GetAxis(_horizontalAxisName);
        _zInput = Input.GetAxis(_verticalAxisName);               
    }
    private void PlayerMovement()
    {
        _direction = transform.right * _xInput + transform.forward * _zInput; // transform.right and transform.forward takes the direction of which player is facing and then goes right
        _playerController.Move(_direction*_movementSpeed*Time.deltaTime);
        _overallSpeed = _playerController.velocity.magnitude; // current player speed
    }
    private void GravityManager()
    {
        _isGrounded = Physics.CheckSphere(_groundChecker.position, _groundDistance, _groundMask); // creates a little sphere under our player which checks what objects we are colliding with
        if (_isGrounded && _velocity.y < _zeroVal)
        {
            _velocity.y = _negativeTwoVal; // forcing our player back to the ground
        }
        if (Input.GetKeyDown(KeyCode.Space)&&_isGrounded==true)
        {
            _velocity.y = Mathf.Sqrt(_jumpHeight * _negativeTwoVal * _gravityForce);
            StartCoroutine(SoundJumpOperator());
        }
        _velocity.y += _gravityForce * Time.deltaTime;
        _playerController.Move(_velocity * Time.deltaTime);
    }
    private void Awake()
    {
        _playerController = this.GetComponent<CharacterController>();
        _feetCanPlay = true;
    }
    private void Update()
    {
        AudioOperator();
        GravityManager();
        InputActivators();
        PlayerMovement();
    }
}