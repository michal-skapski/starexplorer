﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoBannerAnimCont : MonoBehaviour
{
    private static Animator _thisAnimator;

    private static string _infoAnimTrigger = "triggerInfo";
    public static void TriggerAnimation()
    {
        _thisAnimator.SetTrigger(_infoAnimTrigger);
    }

    private void Awake()
    {
        _thisAnimator = this.gameObject.GetComponent<Animator>();
    }
}
