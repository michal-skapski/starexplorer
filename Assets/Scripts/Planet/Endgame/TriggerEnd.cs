﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TriggerEnd : MonoBehaviour
{
    private static Collider _thisCollider;
    private bool _isPlayerInTrigger;
    [SerializeField] private float _fadeToBlackTime;
    private float _timeToWaitForLoad = 0.5f;
    private string _summarySceneName = "Ending_summary";

    //player's components:
    [SerializeField] private GameObject _playerObject;
    private Collider _playerCollider;
    private CharacterController _playerController;

    //UI elements:
    [SerializeField] private TextMeshProUGUI _getInInstruction;
    [SerializeField] private Image _fadeImage;

    public static void ShipTriggerOn()
    {
       _thisCollider.enabled = true;
    }

    //turning elements on/off when player is in/out the trigger
    private void ToggleInTrigger(bool isInTrigger)
    {
        _getInInstruction.enabled = isInTrigger;
        _isPlayerInTrigger = isInTrigger;
    }

    private void Awake()
    {
        //getting player's object components:
        _playerCollider = _playerObject.GetComponent<Collider>();
        _playerController = _playerObject.GetComponent<CharacterController>();

        _getInInstruction.enabled = false;
        _isPlayerInTrigger = false;
        _thisCollider = this.gameObject.GetComponent<BoxCollider>();
        _thisCollider.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == _playerCollider)
        {
            ToggleInTrigger(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other == _playerCollider)
        {
            ToggleInTrigger(false);
        }
    }

    private void Update()
    {
        if (_isPlayerInTrigger && Input.GetKeyDown(KeyCode.E))
        {
            _playerController.enabled = false;
            StartCoroutine(FadeAway());
        }
    }

    private IEnumerator FadeAway()
    {
        for (float i = 0; i <= _fadeToBlackTime; i += Time.deltaTime)
        {
            // set color with i as alpha
            _fadeImage.color = new Color(0, 0, 0, i);
            yield return null;
        }
        yield return new WaitForSeconds(_timeToWaitForLoad);
        SceneManager.LoadScene(_summarySceneName);
    }
}
