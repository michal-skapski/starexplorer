﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlanetUiManager : MonoBehaviour
{
    [SerializeField] private GameObject _questBar;
    [SerializeField] private Image _crosshairImage;
    [SerializeField] private Image _controlsImage;
    [SerializeField] private Image _pauseMenu;
    [SerializeField] private Button _confirmButton;
    [SerializeField] private Button _resumeButton;
    [SerializeField] private Button _quitButton;
    private int _noTime = 0;
    private int _normalTime = 1;
    private string _mainMenuScene = "MainMenu";

    private bool _menuCanBeActive;
    private bool _controlsCanBeActive;

    private void ShowPauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape)&&_menuCanBeActive ==true)
        {
            _controlsCanBeActive = false;
            Time.timeScale = _noTime;
            Cursor.lockState = CursorLockMode.None;
            _pauseMenu.gameObject.SetActive(true);
            _questBar.gameObject.SetActive(false);
            _crosshairImage.gameObject.SetActive(false);
        }
    }
    private void ResumeGame()
    {
        _pauseMenu.gameObject.SetActive(false);
        _questBar.gameObject.SetActive(true);
        _crosshairImage.gameObject.SetActive(true);
        _controlsCanBeActive = true;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = _normalTime;
    }
    private void MainMenuExit()
    {
        SceneManager.LoadScene(_mainMenuScene);
    }
    private void ShowControls()
    {
        if (Input.GetKeyDown(KeyCode.F1) && _controlsCanBeActive == true)
        {
            _menuCanBeActive = false;
            Time.timeScale = _noTime;
            _controlsImage.gameObject.SetActive(true);
            _questBar.gameObject.SetActive(false);
            _crosshairImage.gameObject.SetActive(false);
            Cursor.lockState = CursorLockMode.None;
        }
    }
    private void HideControls()
    {
        _controlsImage.gameObject.SetActive(false);
        _questBar.gameObject.SetActive(true);
        _crosshairImage.gameObject.SetActive(true);
        _menuCanBeActive = true;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = _normalTime;
    }
    private void Awake()
    {
        _resumeButton.onClick.AddListener(ResumeGame);
        _quitButton.onClick.AddListener(MainMenuExit);
        _confirmButton.onClick.AddListener(HideControls);
        _controlsCanBeActive = true;
        _menuCanBeActive = true;
    }
    private void Update()
    {
        ShowPauseMenu();
        ShowControls();
    }
}

