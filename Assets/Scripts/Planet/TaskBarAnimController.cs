﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskBarAnimController : MonoBehaviour
{
    [SerializeField] private Animator _anim;
    [SerializeField] private AudioSource _questBarSwapBackSound;
    [SerializeField] private AudioSource _questBarSwapUpSound;
    private string _boolAnimName = "Swap";
    private void QestBarSwap()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            switch (_anim.GetBool(_boolAnimName))
            {
                case false: _anim.SetBool(_boolAnimName, true);_questBarSwapUpSound.Play() ; break;
                case true: _anim.SetBool(_boolAnimName, false); _questBarSwapBackSound.Play(); break;
            }
        }
    }
    private void Update()
    {
        QestBarSwap();
    }
}
