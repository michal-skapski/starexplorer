﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _quitButton;
    [SerializeField] private Button _creditsButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private GameObject _creditsScreen;

    private const string _cosmosSceneName = "CosmosScene";

    private void SetButtonsActive(bool activation)
    {
        _playButton.gameObject.SetActive(activation);
        _quitButton.gameObject.SetActive(activation);
        _creditsButton.gameObject.SetActive(activation);
    }
    private void PlayFunction()
    {
        SceneManager.LoadScene(_cosmosSceneName);
    }
    private void CreditsFunction()
    {
        SetButtonsActive(false);
        _creditsScreen.SetActive(true);
    }
    private void QuitFunction()
    {
        Application.Quit();
    }
    private void BackFunction()
    {
        _creditsScreen.SetActive(false);
        SetButtonsActive(true);
    }
    private void Start()
    {
        _playButton.onClick.AddListener(PlayFunction);
        _creditsButton.onClick.AddListener(CreditsFunction);
        _quitButton.onClick.AddListener(QuitFunction);
        _backButton.onClick.AddListener(BackFunction);
    }
}
