﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrewmateAnimController : MonoBehaviour
{
    [SerializeField] private float _crewmateActivationTime = 5f;
    [SerializeField] private Animator _myAnimator;

    private string _animationTriggerName = "Activator";
    private IEnumerator CrewmateActivator()
    {
        yield return new WaitForSecondsRealtime(_crewmateActivationTime);
        _myAnimator.SetTrigger(_animationTriggerName);
    }
    private void Awake()
    {
        _myAnimator = this.GetComponent<Animator>();
    }
    private void Start()
    {
        StartCoroutine(CrewmateActivator());
    }
}