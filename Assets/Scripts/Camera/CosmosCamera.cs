﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosmosCamera : MonoBehaviour
{
    private Animator _thisAnimator;
    private const string _forwardButtonDown = "isFlyingForward";

    private void ToggleAnimation(bool animationStatus)
    {
        _thisAnimator.SetBool(_forwardButtonDown, animationStatus);
    }

    private void Awake()
    {
        _thisAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            ToggleAnimation(true);
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            ToggleAnimation(false);
        }
    }
}
