﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SpaceAreaLimit : MonoBehaviour
{
    [SerializeField] private Image _fadeImage;
    [SerializeField] private TextMeshProUGUI _warningText;
    [SerializeField] private float _warningWaitDelay = 2.5f;

    private AudioSource _warningSound;
    private float _fadeIteration = 2f;
    private float _fadeRate = 1.3f;

    private void Awake()
    {
        _warningSound = GetComponent<AudioSource>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<ShipController>())
        {
            StartCoroutine(FadeImage(true, other));
        }
    }

    IEnumerator FadeImage(bool fadeAway, Collider collider)
    {
        //fade from opaque to transparent
        if (fadeAway)
        {
            _warningText.enabled = true;
            _warningSound.Play();
            yield return new WaitForSeconds(_warningWaitDelay);
            _warningText.enabled = false;

            for (float i = _fadeIteration; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                _fadeImage.color = new Color(0, 0, 0, i);
                yield return null;
                collider.transform.position = new Vector3(0, 0, 0);
            }
        }
        //fade from transparent to opaque
        else
        {
            for (float i = 0; i <= _fadeRate; i += Time.deltaTime)
            {
                // set color with i as alpha
                _fadeImage.color = new Color(0, 0, 0, i);
                yield return null;
            }
        }
    }
}
