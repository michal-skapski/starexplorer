﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoaderManager : MonoBehaviour
{
    [SerializeField] private Collider _landPlanetColl;
    [SerializeField] private Image _fadeImage;

    private string _firstPlanetSceneName = "FirstPlanetScene";
    private string _boolName = "triggerLanding";

    private Animator _cameraAnimator;

    public bool isAnimationTriggered;

    private float _waitForAnimTime = 2.2f;
    private float _blackoutFade = 1.7f;
    private float _waitBeforeLoad = 0.5f;

    private void Awake()
    {
        isAnimationTriggered = false;
        _cameraAnimator = GetComponentInChildren<Camera>().gameObject.GetComponent<Animator>();
        _cameraAnimator.SetBool(_boolName, false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == _landPlanetColl && !isAnimationTriggered)
        {
            StartCoroutine(SceneLoader());
        }
    }
    private IEnumerator SceneLoader()
    {
        isAnimationTriggered = true;
        _cameraAnimator.SetBool(_boolName, true);
        yield return new WaitForSeconds(_waitForAnimTime);

        for (float i = 0; i <= _blackoutFade; i += Time.deltaTime)
        {
            _fadeImage.color = new Color(0, 0, 0, i);
            yield return null;
        }
        yield return new WaitForSeconds(_waitBeforeLoad);
        SceneManager.LoadScene(_firstPlanetSceneName);
    }
}
