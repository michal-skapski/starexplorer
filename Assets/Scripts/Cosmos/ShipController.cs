﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    //for engine sound
    private AudioSource _engineSoundEffect;
    private bool _isSoundPlaying;
    [SerializeField] private float _volumeChangeRate;

    //for level loading animation purposes
    private int _landingSpeed = 4;
    private LoaderManager _forLvlLoaderAnim;

    [SerializeField] private float _forwardSpeed = 25f;
    [SerializeField] private float _rotationSpeed = 10f;
    [SerializeField] private float _horizontalSpeed = 25f;
    [SerializeField] private float _boostSpeed = 25f;
    [SerializeField] private float _lookRateSpeed = 90f;
    //Accelaration Variables
    [SerializeField] private float _forwardAccelartion = 2.5f, _rotationAcceleration = 2f, _hoverAccelaration = 2f;

    // Float Variables so there is never an magic numbers

    private float _zeroVal = 0f;
    private float _halfVal = 0.5f;
    private float _oneVal = 1f;

    private float _activeForwardSpeed, _activeRotationSpeed,  _activeHorizontalSpeed, _activeBoostSpeed; // used for input - numbers beetween -1 and 1

    //Those strings are used so there was no magic strings
    private const string _verticalAxisName = "Vertical"; // used const for strings to read only
    private readonly string _rotationalAxisName = "Rotational"; // or read only
    private string _horizontalAxisName = "Horizontal";
    private string _boostAxisName = "Boost";

    private Rigidbody _rb;
    private Vector2 _mouseInput, _screenCenter, _mouseDistance;

    private bool _isMoving;

    //for checking if player is moving the ship
    private bool MoveDetection
    {
        get
        {
            if (Input.GetAxis(_verticalAxisName) != 0 || Input.GetAxis(_horizontalAxisName) != 0 || Input.GetAxis(_boostAxisName) != 0)
            {
                _isMoving = true;
            }
            else
            {
                _isMoving = false;
            }
            return _isMoving;
        }
    }

    private void EngineSound()
    {
        if (MoveDetection && !_isSoundPlaying)
        {
            _isSoundPlaying = true;
            StartCoroutine(VolumeUp());
        }
        else if (!MoveDetection && _isSoundPlaying)
        {
            _isSoundPlaying = false;
            StartCoroutine(VolumeDown());
        }
    }

    private void InputActivators()
    {
        // Input Activators - input + speed + accelarations

        //Mathf.Lerp(from current number, to current number, how fast)

        _activeForwardSpeed = Mathf.Lerp(_activeForwardSpeed, Input.GetAxisRaw(_verticalAxisName) * _forwardSpeed, _forwardAccelartion * Time.deltaTime);
        _activeRotationSpeed = Mathf.Lerp(_activeRotationSpeed, Input.GetAxisRaw(_rotationalAxisName) * _rotationSpeed, _rotationAcceleration * Time.deltaTime);
        _activeHorizontalSpeed = Mathf.Lerp(_activeHorizontalSpeed, Input.GetAxisRaw(_horizontalAxisName) * _horizontalSpeed, _hoverAccelaration * Time.deltaTime);
        _activeBoostSpeed = Input.GetAxisRaw(_boostAxisName) * _boostSpeed; // don't want to use accelaration when moving up and down

        // Mouse input

        _mouseInput.x = Input.mousePosition.x;
        _mouseInput.y = Input.mousePosition.y;

        _mouseDistance.x = (_mouseInput.x - _screenCenter.x) / _screenCenter.y;
        _mouseDistance.y = (_mouseInput.y - _screenCenter.y) / _screenCenter.y;


    }
    private void ShipMovement()
    {
        _rb.velocity = Vector3.ClampMagnitude(_rb.velocity, _forwardSpeed); // clamping the value of speed

        // Movement - adding force to an object in certain way (input) * _speed
        _rb.AddForce(transform.forward * _activeForwardSpeed);
        _rb.AddForce(transform.right * _activeHorizontalSpeed);
        _rb.AddForce(transform.up * _activeBoostSpeed);


        // Clamping the mouseDistance so that we can never speed to much based on our mouse location
        _mouseDistance = Vector2.ClampMagnitude(_mouseDistance, _oneVal);

        // Rotation Activators - rotate * (input * speed) * time
        transform.Rotate(-_mouseDistance.y * _lookRateSpeed * Time.fixedDeltaTime, _mouseDistance.x * _lookRateSpeed * Time.fixedDeltaTime, _activeRotationSpeed * _rotationSpeed * Time.fixedDeltaTime, Space.Self);
    }

    //these two coroutines are for engine sound volume management
    IEnumerator VolumeUp()
    {
        _engineSoundEffect.volume = 0;
        _engineSoundEffect.Play();

        for (float i = _zeroVal; i <= _volumeChangeRate; i += Time.deltaTime)
        {
            _engineSoundEffect.volume = i;
            yield return null;
        }
    }
    IEnumerator VolumeDown()
    {
        for (float i = _volumeChangeRate; i >= _zeroVal; i -= Time.deltaTime)
        {
            _engineSoundEffect.volume = i;
            yield return null;
        }

        _engineSoundEffect.volume = 0;
        _engineSoundEffect.Pause();
    }

    private void OnTriggerEnter(Collider other)
    {
        _rb.velocity = Vector3.ClampMagnitude(_rb.velocity, _zeroVal); // reducing the speed and rotation when hitting an object
    }
    private void Awake()
    {
        _isSoundPlaying = false;
        _engineSoundEffect = GetComponent<AudioSource>();
        _engineSoundEffect.volume = _zeroVal;

        _forLvlLoaderAnim = gameObject.GetComponent<LoaderManager>();

        _rb = this.GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Confined; // locks cursor in the game window

        // Measuring the sreen center by divide it's height and with by two

        _screenCenter.x = Screen.width * _halfVal;
        _screenCenter.y = Screen.height * _halfVal;
    }
    private void Update()
    {
        if (!_forLvlLoaderAnim.isAnimationTriggered)
        {
            InputActivators();
            EngineSound();
        }
    }
    private void FixedUpdate()
    {
        if (!_forLvlLoaderAnim.isAnimationTriggered)
        {
            ShipMovement();
        } else
        {
            _rb.AddForce(transform.forward * _landingSpeed);
        }
    }
}