﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoreUiManager : MonoBehaviour
{
    [SerializeField] private AudioSource _mainAmbient;
    [SerializeField] private TextMeshProUGUI[] _loreTexts;
    [SerializeField] private TextMeshProUGUI _welcomeText;
    [SerializeField] private TextMeshProUGUI _buttonText;
    [SerializeField] private Button _nextButton;
    [SerializeField] private Image _loreImages;

    private int _zeroVal=0;
    private int _oneVal = 1;
    private int _threeVal = 3;
    private int _currentVal = 0;

    private string _nextText = "Next";
    private string _thanksText = "Thank you";

    public bool menuCanBeActive;
    public bool controlsCanBeActive;

    private void NextTextFunction()
    {
        if(_currentVal < _loreTexts.Length-_oneVal)
        {
            _loreTexts[_currentVal].gameObject.SetActive(false);
            _currentVal += _oneVal;
            if (_currentVal == _threeVal)
            {
                _buttonText.text = _thanksText;
            }
            _loreTexts[_currentVal].gameObject.SetActive(true);

        }
        else
        {
            menuCanBeActive = true;
            controlsCanBeActive = true;
            _loreImages.gameObject.SetActive(false);
            Time.timeScale = _oneVal;
            _mainAmbient.Play();
        }
    }
    private void Awake()
    {
        menuCanBeActive = false;
        controlsCanBeActive = false;
        _nextButton.onClick.AddListener(NextTextFunction);
        _buttonText.text = _nextText;
    }
    private void Start()
    {
        Time.timeScale = _zeroVal;
    }
}
