﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private int _fpsTarget = 60;
    private void Awake()
    {
        Application.targetFrameRate = _fpsTarget; // targeting game fps to 60
    }
}
