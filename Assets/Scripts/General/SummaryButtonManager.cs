﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SummaryButtonManager : MonoBehaviour
{
    [SerializeField] private Button _mainMenuButton;
    private AudioSource _buttonAudio;
    private string _menuSceneName = "MainMenu";

    private void LoadMenu()
    {
        _buttonAudio.Play();
        SceneManager.LoadScene(_menuSceneName);
    }

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Confined;
        _buttonAudio = _mainMenuButton.GetComponent<AudioSource>();
        _mainMenuButton.onClick.AddListener(LoadMenu);
    }
}
