﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CosmosUiManager : MonoBehaviour
{
    [SerializeField] private LoreUiManager _loreManager;
    [SerializeField] private Image _controlsImage;
    [SerializeField] private Image _pauseMenu;
    [SerializeField] private Button _confirmButton;
    [SerializeField] private Button _resumeButton;
    [SerializeField] private Button _quitButton;
    [SerializeField] private AudioSource _mainAmbient;
    private int _noTime = 0;
    private int _normalTime = 1;
    private string _mainMenuScene = "MainMenu";


    private void ShowPauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && _loreManager.menuCanBeActive == true)
        {
            Time.timeScale = _noTime;
            _mainAmbient.Pause();
            _controlsImage.gameObject.SetActive(false);
            _pauseMenu.gameObject.SetActive(true);
        }
    }
    private void ResumeGame()
    {
        _pauseMenu.gameObject.SetActive(false);
        Time.timeScale = _normalTime;
        _mainAmbient.UnPause();
        _loreManager.menuCanBeActive = true;
        _loreManager.controlsCanBeActive = true;
    }
    private void MainMenuExit()
    {
        SceneManager.LoadScene(_mainMenuScene);
    }
    private void ShowControls()
    {
        if (Input.GetKeyDown(KeyCode.F1) && _loreManager.controlsCanBeActive == true)
        {
            Time.timeScale = _noTime;
            _controlsImage.gameObject.SetActive(true);
        }
    }
    private void HideControls()
    {
        _controlsImage.gameObject.SetActive(false);
        Time.timeScale = _normalTime;
        _loreManager.controlsCanBeActive = true;
        _loreManager.menuCanBeActive = true;
    }
    private void Awake()
    {
        _resumeButton.onClick.AddListener(ResumeGame);
        _quitButton.onClick.AddListener(MainMenuExit);
        _confirmButton.onClick.AddListener(HideControls);
    }
    private void Update()
    {
        ShowPauseMenu();
        ShowControls();
    }
}
